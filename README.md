# Control Plane Experiments
Not having access to master nodes on GKE made it somewhat difficult to test interesting features (like [webhook token authentication][feature/heptio-authenticator]), so I threw together the parts to do it myself. This repo strives to provide a Kubernetes control plane (apiserver, scheduler, and controller manager) that is secure, similar to production, and easy to tweak.

Most of the interesting work is done on unmerged branches, be sure to check them out!

[feature/heptio-authenticator]: https://gitlab.com/wmedlar/control-plane/tree/feature/heptio-authenticator

## Quickstart
- Run `make` to generate all the necessary public key infrastructure and kubeconfigs.
- Run `docker-compose up -d` to spin up the control plane containers.

In less than a minute your control plane should be live and ready to receive incoming requests. You can use kubectl to confirm the "cluster" is healthy.

```shell
$ kubectl --kubeconfig configs/admin.yaml get componentstatuses
NAME                 STATUS      MESSAGE                                                                                        ERROR
controller-manager   Unhealthy   Get http://127.0.0.1:10252/healthz: dial tcp 127.0.0.1:10252: getsockopt: connection refused
scheduler            Unhealthy   Get http://127.0.0.1:10251/healthz: dial tcp 127.0.0.1:10251: getsockopt: connection refused
etcd-0               Healthy     {"health": "true"}
```

Until issue #6 is resolved the controller-manager and scheduler will be reported as unreachable by the apiserver. You can confirm they're both healthy by hitting their healthcheck endpoints from the host machine.

```shell
$ curl -fw "\n" http://localhost:10251/healthz # scheduler
ok
$ curl -fw "\n" http://localhost:10252/healthz # controller-manager
ok
```

You can also use `etcdctl` to query the status of our single-node etcd cluster directly.

```shell
$ etcdctl --endpoints https://localhost:2379 --cert-file pki/etcd-client.pem --key-file pki/etcd-client.key --ca-file pki/ca.pem cluster-health
failed to check the health of member 8e9e05c52164694d on https://etcd:2379: Get https://etcd:2379/health: dial tcp: lookup etcd on 192.168.1.1:53: no such host
member 8e9e05c52164694d is healthy: got healthy result from https://localhost:2379
cluster is healthy
```

## Heptio Authenticator

* See [feature/heptio-authenticator].

[Heptio's Authenticator][heptio-authenticator] is a token authentication webhook server that uses Amazon's IAM as an identity provider. With Authenticator you can map IAM users or roles to Kubernetes users. For example, the configuration snippet below would map IAM user `arn:aws:iam::000000000000:user/Alice` to a Kubernetes user with the name `alice` in the `system:masters` (admin) group.

```yaml
server:
  mapUsers:
    - userARN: arn:aws:iam::000000000000:user/Alice
      username: alice
      groups:
        - system:masters
```

Authenticator expects to run with host networking, and is hardcoded to listen on `127.0.0.1`. This can prevent a challenge for some Kubernetes setups (e.g., running the apiserver as a static pod _without_ host networking), but can be done simply with docker-compose's service `network_mode` (see [this][docker-run-network-container] and [this][docker-compose-network-mode]).

On its first run Authenticator will need to generate PKI and a kubeconfig referenced by the apiserver. If the kubeconfig file doesn't exist when the apiserver is spun up it will crash, sometimes leading to issues with Authenticator's network interface, making it unreachable even when all containers are healthy. To alleviate this issue run Authenticator first to allow it to bootstrap.

```shell
$ docker-compose up --no-deps authenticator
Creating controlplane_authenticator_1 ... done
Attaching to controlplane_authenticator_1
authenticator_1       | time="2018-03-29T04:50:30Z" level=info msg="mapping IAM role" groups="[system:masters]" role="arn:aws:iam::000000000000:role/KubernetesAdmin" username="admin:{{SessionName}}"
authenticator_1       | time="2018-03-29T04:50:31Z" level=info msg="generated a new private key and certificate" certBytes=810 keyBytes=1191
authenticator_1       | time="2018-03-29T04:50:31Z" level=info msg="saving new key and certificate" certPath=/etc/heptio-authenticator/cert.pem keyPath=/etc/heptio-authenticator/key.pem
authenticator_1       | time="2018-03-29T04:50:31Z" level=info msg="loaded existing keypair" certPath=/etc/heptio-authenticator/cert.pem keyPath=/etc/heptio-authenticator/key.pem
authenticator_1       | time="2018-03-29T04:50:31Z" level=info msg="writing webhook kubeconfig file" kubeconfigPath=/etc/heptio-authenticator/webhook.yaml
authenticator_1       | time="2018-03-29T04:50:31Z" level=info msg="listening on https://127.0.0.1:21362/authenticate"
authenticator_1       | time="2018-03-29T04:50:31Z" level=info msg="reconfigure your apiserver with `--authentication-token-webhook-config-file=/etc/heptio-authenticator/webhook.yaml` to enable (assuming default hostPath mounts)"
$ docker-compose up -d
Creating controlplane_etcd_1               ... done
Creating controlplane_cacerts_1            ... done
Creating controlplane_apiserver_1          ... done
Creating controlplane_controller-manager_1 ... done
Recreating controlplane_authenticator_1 ... done
```

Because of its unusual network requirements, Authenticator can't be exposed to the host, but we can reach it through the apiserver.

```shell
$ heptio-authenticator-aws --cluster-id k8s.control-plane.internal --config config.yaml token
k8s-aws-v1.aHR[...]jAz
$ docker-compose exec apiserver wget \
    -qO- \
    --header 'Content-Type:application/json' \
    --post-data '{"apiVersion": "authentication.k8s.io/v1beta1", "kind": "TokenReview", "spec": {"token": "k8s-aws-v1.aHR[...]jAz"}}' \
    https://127.0.0.1:21362/authenticate
{"metadata":{"creationTimestamp":null},"spec":{},"status":{"authenticated":true,"user":{"username":"wmedlar","uid":"heptio-authenticator-aws:0123456789:ABCDEFGHIJKLMNOPQRSTU","groups":["system:masters"]}}}
```

Check out [Authenticator's documentation][heptio-authenticator] for more info on how to configure and use it.

[heptio-authenticator]: https://github.com/heptio/authenticator
[docker-run-network-container]: https://docs.docker.com/engine/reference/run/#network-container
[docker-compose-network-mode]: https://docs.docker.com/compose/compose-file/#network_mode
