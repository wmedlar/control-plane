# Kubernetes Public Key Infrastructure
This directory contains the OpenSSL configs used to generate the public key infrastructure and X509 certificates for each Kubernetes component.

## Quickstart
Run `make` to generate the necessary keys and certs.

## Generated Files
### Certificate Authority
The certificate authority is the root of trust in the cluster; all cluster components present certificates signed by this authority. The root certificate itself is created as `ca.pem`, with the matching private key as `ca.key`.

The certificate and key pair have many uses in the cluster, including:
- signing client certificates (`kube-apiserver --client-ca-file`)
- verifying etcd identity (`kube-apiserver --etcd-cafile`)
- verifying apiserver identity (`etcd --trusted-ca-file`)
- [issue certificates from CertificateSigningRequest resources][managing-tls-in-a-cluster] (`kube-controller-manager --cluster-signing-cert-file` and `--cluster-signing-key-file`)
- included in service account token secrets (`kube-controller-manager --root-ca-file`)
- sign service account tokens (`kube-controller-manager --service-account-private-key-file`)

`make` also generates a serial number file, `ca.srl`, and will be increment it each time a new certificate is signed.

### TLS Assets
All communication within the cluster is with either etcd or the apiserver, and is encrypted over TLS. Etcd and the apiserver both present certificates to verify their identities, `kube-apiserver --tls-cert-file` and `--tls-private-key-file`, `etcd --cert-file` and `--key-file`, respectively.
- `apiserver.key` and `apiserver.pem`
- `etcd-server.key` and `etcd-server.pem`.

### Client Authentication
Authentication in the cluster is facilitated by client certificates signed by the cluster authority. Kubeconfigs referencing these files, as well as the ... are created by running `make configs` in the top-level directory of the repository.

The admin pair are used to authenticate with the apiserver as the `admin` user, a member of the `system:masters` group. This user has full access to the cluster.
- `admin.key` and `admin.pem`

The etcd client pair are used by the apiserver to authenticate with etcd (`kube-apiserver --etcd-certfile` and `--etcd-keyfile`).
- `etcd-client.key` and `etcd-client.pem`

The controller-manager and scheduler authenticate with the apiserver with their respective file pairs.
- `controller-manager.key` and `controller-manager.pem`
- `scheduler.key` and `scheduler.pem`

[managing-tls-in-a-cluster]: https://kubernetes.io/docs/tasks/tls/managing-tls-in-a-cluster/
