CONFIGS := admin controller-manager scheduler

.PHONY: all
all: pki configs

.PHONY: pki
pki:
	cd pki/ && $(MAKE) pki

pki/%:
	cd pki/ && $(MAKE) "$@"

.PHONY: configs $(CONFIGS)
configs: $(CONFIGS)

$(CONFIGS): $(patsubst %,configs/%.yaml,$(CONFIGS))

configs/%.yaml: pki/ca.pem pki/%.pem pki/%.key
	kubectl config --kubeconfig="$@" set-cluster default --server=https://127.0.0.1:6443 --certificate-authority=pki/ca.pem --embed-certs=true
	kubectl config --kubeconfig="$@" set-credentials "$*" --client-certificate="pki/$*.pem" --client-key="pki/$*.key" --embed-certs=true
	kubectl config --kubeconfig="$@" set-context default --cluster=default --user="$*"
	kubectl config --kubeconfig="$@" use-context default
